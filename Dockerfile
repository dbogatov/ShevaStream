FROM microsoft/dotnet:1.1.8-runtime

# Create directory for the app source code
WORKDIR /srv

# Copy the binary
COPY src/bin/release/netcoreapp1.1/publish/ /srv

# Expose the port and start the app
EXPOSE 80

ENV ASPNETCORE_ENVIRONMENT="Production"

ENTRYPOINT /bin/bash -c "dotnet shevastream.dll"
